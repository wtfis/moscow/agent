# Imports
import pymysql, pymysql.cursors

# DB-connctions
def dbConnection():
    connection = pymysql.connect(host='paurix.de',
                             user='moscow_writer',
                             password='Secure01',
                             port=33666,
                             database='monDB',
                             cursorclass=pymysql.cursors.DictCursor)
    return connection

# Write to DB
def dbWrite(myhost,mem,cpu):
    with dbConnection() as connection:      # Start connection, gets automaticially closed due to use of 'with'
        with connection.cursor() as cursor:
            # Create a new record
            sql = f"INSERT INTO monData (host, ramPercent, cpuPercent) VALUES ('{myhost}', {mem}, {cpu})"
            cursor.execute(sql)
        connection.commit()
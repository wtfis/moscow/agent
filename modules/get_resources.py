### Imports
import psutil

### Functions
def disk_linux():
    parts = psutil.disk_partitions()
    host_partitions = []
    for part in parts:
        if part.fstype != 'squashfs':   # Filter out snap-containers
            partition_usage = psutil.disk_usage(part.mountpoint)   # Grab usage info about partition in scope
            partition_details = [
                f"device=       {part.device}",
                f"mountpoint=   {part.mountpoint}",
                f"total=        {partition_usage.total}",
                f"free=         {partition_usage.free}",
                f"percent=      {partition_usage.percent}"
            ]

            # Append data about the partition in scope to datalist about all partitions
            host_partitions.append(partition_details)

def users() -> list:    # '-> list' = Hint, welcher Typ returned wird
    logged_in_users = psutil.users()  # Grabbing all logged-in users
    userlist = []
    
    for user_scope in range(0,len(logged_in_users)):    # iterating through all users 
        userlist.append(logged_in_users[user_scope].name)   # extracting usernames from returned data

    return userlist

def cpu_utilization() -> float:
    cpu_percent = psutil.cpu_percent(interval=1)    # cpu-utilization average for $interval (filtering out peaks)
    
    return cpu_percent

def memory():
    mem = psutil.virtual_memory()
    
    return mem
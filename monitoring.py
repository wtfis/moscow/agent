#!/usr/bin/env python3

### Imports
import logging                          # Logging-library: Output to console + logfile
import socket                           # Grabbing hostname
import time
import itertools                        # Easier time keeping track of iteration in the main-loop
import modules.get_resources as ress    # Our module for grabbing data abt respective host
import modules.dataToDB as db           # Sending data to db
import argparse as arg                  # Argument parsing

### Variables
myhost = socket.gethostname()   # get Hostname

### Functions
# Argument parsing + help
def usage():
    """How to use this script & argument parsing"""
    parser_main = arg.ArgumentParser(   # Constructing the argument-parser
        add_help=False,                 # Disable default help (would get shown on top, want it at bottom)
        description="Monitors certain hardware-data about this host and sends the data to a central database",
        usage="monitoring.py [<args>]",
        epilog="EXAMPLE: monitoring.py --tests CPU, RAM --loglevel INFO --iterations 2"
    )
   
    optional = parser_main.add_argument_group('optional arguments') # Collecting all optional arguments

    # Adding arguments
    optional.add_argument("-t", "--tests", required=False, type=str, nargs="+",
        choices= ["CPU", "RAM"],
        default= ["CPU", "RAM"],
        help="Whistespace seperated list, which tests are being executed")
    optional.add_argument("-ll", "--loglevel", required=False, type=str,
        choices= ["DEBUG", "INFO", "WARNING", "CRITICAL"],
        default= "DEBUG",
        help="Set console-log-level. [DEBUG -> INFO -> WARNING -> CRITICAL]")
    optional.add_argument("-i","--iterations", required=False, type=int,
        default=0,
        help="How often the check-loop should be executed! Default = until stopped!")
    # Add back help at bottom
    optional.add_argument('-h', '--help', action='help', help='Show this help message and exit')
       
    return parser_main.parse_args()

# Logging setup
def setup_logger(loglevel:str):                     # 'loglevel:str' -> displays what type the var 'loglevel' is supposed to be while calling function
    '''
    Constructs a logger object which log messages can be passed to

        Parameters:
            loglevel   (str): Loglevel of console-output (DEBUG -> INFO -> WARNING -> CRITICAL)
    '''
    logger = logging.getLogger(myhost)              # Create base logger with name of target host
    logger.setLevel("DEBUG")

    formatter = logging.Formatter(                  # Create baseline for format of log messages
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    console_handler = logging.StreamHandler()       # Handler for console-output (std.err by default)
    console_handler.setLevel(f"{loglevel.upper()}") # Set minimum log-level for console output (DEBUG -> INFO -> WARNING -> CRITICAL) from argument.
    console_handler.setFormatter(formatter)         # Apply format to this handler
    if loglevel != "DEBUG": # => non default
        print(f"Console log-level {loglevel.upper()} selected!")
    logger.addHandler(console_handler)              # Add console-handler to logger
    
    file_handler = logging.FileHandler(             # Handler for logfile-output
        filename=f"{myhost}.log",                   # Where the logfile should be created
        mode="a"                                    # Write in append-mode
        )
    file_handler.setLevel("WARNING")                # Write only WARNING and above to the logfile
    file_handler.setFormatter(formatter)            # Apply format to this handler
    logger.addHandler(file_handler)                 # Add file-handler to logger

    return logger

# Grab data from module.get_ressources, check against thresholds.
def check_limits(logger):
    # Logged-in-users
    userlist = ress.users()
    logger.info('Logged in Users: ' + ', '.join(userlist))  # constructing string for log -> logfile & console
    
    # CPU utilization in percent
    cpu_percent = ress.cpu_utilization() # get info about cpu-utilization
    if cpu_percent > 90:
        logger.critical('CPU percentage at ' + str(cpu_percent) + ' %')
    elif cpu_percent > 80:
        logger.warning('CPU percentage at ' + str(cpu_percent) + ' %')
    else:
        logger.info('CPU percentage at ' + str(cpu_percent) + ' %')
    
    # RAM utilization in percent
    mem = ress.memory()
    if mem.percent > 90:
        logger.critical('RAM percentage at ' + str(mem.percent) + ' %')
    elif mem.percent > 80:
        logger.warning('RAM percentage at ' + str(mem.percent) + ' %')
    else:
        logger.info('RAM percentage at ' + str(mem.percent) + ' %')

    return cpu_percent, mem.percent
    
### Main
# Main loop, grabs data w/ help of psutil and checks for thresholds
def main(args):
    try:
        logger = setup_logger(args.loglevel)    # Setup logger with handler for logfile and console-output
    except Exception as e:
                    print(f"\nError setting up the logger:\n{e}")

    for iteration in itertools.count():         # Replaces the "While True" loop. We get a count for how often the code has run already
        try:
            if iteration != 0:
                if args.iterations != 0 and args.iterations <= iteration: # Breaks the loop, when it ran more often than specified in the argument
                    break
                time.sleep(60)                      # If not the first iteration, sleep for 60s -> 1 report / min
        
            cpu_util, mem_util = check_limits(logger)   # Grabs performance-data, compares it to thresholds, writes to console and logfile if above
            db.dbWrite(myhost, mem_util, cpu_util)      # Writes data into database

        except KeyboardInterrupt:
            print("\nKeyboard interrupt! Exiting...")
            break
        except Exception as e:
            print(f"\nError:\n{e}")
            break

if __name__ == "__main__":      # If script gets called / started as a non-import, execute!
    args = usage()              # Parse arguments
    main(args)                  # Call main, passing arguments
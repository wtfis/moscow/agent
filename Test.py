import unittest
import modules.dataToDB as db

connection = db.dbConnection()

class TestDB(unittest.TestCase):
    def test_db(self):
        self.assertEqual(connection.open, True)


if __name__ == '__main__':
    unittest.main()